package com.sahil.privatenotes.Adaptors

import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.sahil.privatenotes.ActivityPack.AddNotesActivity
import com.sahil.privatenotes.CallBack.NotesCallBack
import com.sahil.privatenotes.R
import java.io.File
import java.text.DateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * @Author : Sahil Yadav
 * @Date : 07-04-2022
 * @Email : rao.sahab504@gmail.com
 */
class RvNoteListAdoptor : RecyclerView.Adapter<RvNoteListAdoptor.MyViewHolder>() {

    private var fullList: MutableList<File>
    private var filesList: MutableList<File>

    init {
        filesList = ArrayList()
        fullList = ArrayList()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =LayoutInflater.from(parent.context)
            .inflate(R.layout.rv_notes_list_layout,parent,false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val file : File = filesList[position]
        val fileName : String = file.name.substring(0,file.name.length -4)
        val updateDate : String = DateFormat.getDateInstance(DateFormat.MEDIUM).format(file.lastModified())
        val updatedTime : String = DateFormat.getTimeInstance(DateFormat.SHORT).format(file.lastModified())

        holder.setData(fileName,updateDate,updatedTime)
    }

    override fun getItemCount(): Int {
        return filesList.size
    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) ,View.OnClickListener {
        var stringTitle : String = ""
        val tv_notes_title: TextView = itemView.findViewById(R.id.tv_notes_title)
        val tv_notes_created: TextView = itemView.findViewById(R.id.tv_notes_created)
        val tv_notes_updated: TextView = itemView.findViewById(R.id.tv_notes_updated)
        val cv_notes_item: CardView = itemView.findViewById(R.id.cv_notes_item)


        fun setData(title: String, date: String, time: String) {
            stringTitle = title;
            tv_notes_title.setText(stringTitle)
            tv_notes_created.setText(date)
            tv_notes_updated.setText(time)

            cv_notes_item.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            itemView.context.startActivity(AddNotesActivity().getStartIntent(itemView.context,stringTitle))


        }
    }
    fun updateList(file : MutableList<File> ){
        filesList=file
        fullList = ArrayList(filesList)
    }
    fun deleteFile(position : Int){
        val file : File = filesList.get(position)
        fullList.remove(file)
        filesList.remove(file)
        notifyItemChanged(position)
        file.delete()
    }
    fun cancelDelete(posotion : Int){
        notifyItemChanged(posotion)
    }

    fun filterList(query :String){
        if (TextUtils.isEmpty(query)){
            DiffUtil.calculateDiff(NotesCallBack(fullList,filesList)).dispatchUpdatesTo(this)
            filesList = ArrayList(fullList)
        }else{
            filesList.clear()
            for (item in fullList.indices){
                val file : File =fullList.get(item)
                val fileName : String = file.name.substring(0,file.name.length-4).lowercase()
                if (fileName.contains(query)){
                    filesList.add(fullList[item])
                }
            }
            DiffUtil.calculateDiff(NotesCallBack(fullList,filesList)).dispatchUpdatesTo(this)
        }
    }
}