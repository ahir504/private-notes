package com.sahil.privatenotes.ActivityPack

/**
 * @Author : Sahil Yadav
 * @Date : 06-04-2022
 * @Email : rao.sahab504@gmail.com
 */
class Notes (
    val title : String,
    val discription : String,
    val createdAt : String,
    val updatedAt : String
        )
