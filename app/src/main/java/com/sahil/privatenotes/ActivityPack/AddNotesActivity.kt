package com.sahil.privatenotes.ActivityPack

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.sahil.privatenotes.R
import com.sahil.privatenotes.Utils.Utills

class AddNotesActivity : AppCompatActivity() {

    private var etTitle : EditText? = null
    private var etDiscription : EditText? = null
    private var stTitle : String? = null
    private var stDiscriptaion : String? = null
    private val utills = Utills()
    private var dialog : AlertDialog? = null
    private val EXTRA_NOTE_TITLE = "EXTRA_NOTE_TITLE"

    fun getStartIntent(context: Context?, title: String?): Intent {
        val intent = Intent(context, AddNotesActivity::class.java)
        intent.putExtra(EXTRA_NOTE_TITLE, title)
        return intent
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_notes)



        etTitle = findViewById(R.id.et_title)
        etDiscription =findViewById(R.id.et_discription)

        val intent : Intent = intent
        val action : String? = intent.action
        val type : String? = intent.type

        if (Intent.ACTION_SEND.equals(action) && type != null){
            if ("text/plain".equals(type)){
                val text = intent.getStringExtra(Intent.EXTRA_TEXT)
                etDiscription!!.setText(text)
                stDiscriptaion =text
                stTitle = ""

            }
        }else{
            stTitle = intent.getStringExtra(EXTRA_NOTE_TITLE)
     //       Toast.makeText(this,"${intent.getStringExtra(EXTRA_NOTE_TITLE)}",Toast.LENGTH_SHORT).show()
            if (stTitle == null || TextUtils.isEmpty(stTitle)) {
                stTitle = ""
                stDiscriptaion = ""
                etDiscription!!.requestFocus()
                if (supportActionBar != null)
                    supportActionBar!!.title = getString(R.string.new_note)
            }else{
                    etTitle!!.setText(stTitle)

                stDiscriptaion = utills.readFile(this, stTitle!!)
                if (stDiscriptaion == ""){
                      Toast.makeText(this,"empty",Toast.LENGTH_SHORT).show()
                    etDiscription!!.hint = "This Note is empty"

                    }else {

                    //  Toast.makeText(this,stDiscriptaion,Toast.LENGTH_SHORT).show()
                    etDiscription!!.setText(stDiscriptaion)
                }
                    if (supportActionBar != null){
                        supportActionBar!!.title = stTitle

                    }
                }

        }
    }

    override fun onRestart() {
        super.onRestart()
        stDiscriptaion = etDiscription?.text.toString().trim()
        currentFocus?.clearFocus()
    }

    override fun onPause() {
        if (!isChangingConfigurations){
            saveFile()
        }
        super.onPause()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java))
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun saveFile(){

        var newTitle : String = etTitle?.text.toString().trim()
        val newDiscription : String  = etDiscription?.text.toString().trim()

      if (TextUtils.isEmpty(newTitle) && TextUtils.isEmpty(newDiscription)){
          return

        }

        if (newTitle.equals(stTitle) && newDiscription.equals(stDiscriptaion)){
            return
        }

        if (!stTitle.equals(newTitle) || TextUtils.isEmpty(newTitle)){
            newTitle = newFileName(newTitle)
            etTitle?.setText(newTitle)
        }

        utills.writeFile(this,newTitle,newDiscription)
        if (!TextUtils.isEmpty(stTitle ) && !newTitle.equals(stTitle)) {
            deleteFile(stTitle + EXTRA_NOTE_TITLE)
        }
        stTitle=newTitle
    }

     fun newFileName(newTitle: String): String {
         var newTitle1 = newTitle
         if(TextUtils.isEmpty(newTitle1)){
            newTitle1=getString(R.string.note_title)
         }
         if (utills.fileExists(this,newTitle1)){
             var i = 1
             while (true){
                 if (!utills.fileExists(this,"$newTitle1($i)") || stTitle.equals("$newTitle1($i)")){
                     newTitle1 = ("$newTitle1($i)")
                     break
                 }
                 i++
             }
         }
         return newTitle1
    }
}