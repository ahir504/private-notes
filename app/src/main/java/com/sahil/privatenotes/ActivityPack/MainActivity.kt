package com.sahil.privatenotes.ActivityPack

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.sahil.privatenotes.Adaptors.RvNoteListAdoptor
import com.sahil.privatenotes.R
import com.sahil.privatenotes.Utils.Utills

class MainActivity : AppCompatActivity(), SearchView.OnQueryTextListener {
    var rv_notes_list : RecyclerView? = null
    var fab_add_notes : FloatingActionButton? = null
    var tvEmptyList : TextView? = null
    val rvNoteListAdoptor = RvNoteListAdoptor()
    val uitil = Utills()
    val addNotesActivity = AddNotesActivity()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rv_notes_list = findViewById(R.id.rv_notes_list)
        fab_add_notes = findViewById(R.id.fab_add_note)
        tvEmptyList = findViewById(R.id.tv_empty_list)




        val linearLayoutManager = LinearLayoutManager(this)
        rv_notes_list?.layoutManager = linearLayoutManager

        fab_add_notes?.setOnClickListener { view ->
            run {
                startActivity(addNotesActivity.getStartIntent(this,""))
                finishAffinity()
            }
        }
    }

    override fun onBackPressed() {


        val searchView : SearchView = findViewById(R.id.btn_search)
        if (!searchView.isIconified){
            searchView.onActionViewCollapsed()
            finishAffinity()
        }else{
            super.onBackPressed()
            finishAffinity()
        }
    }
    override fun onQueryTextChange(query: String): Boolean {
        rvNoteListAdoptor.filterList(query.lowercase())
        return true
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onResume() {
        super.onResume()
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        val search   = findViewById<SearchView>(R.id.btn_search)
        if (search != null){
            if (!search.isIconified){
                search.onActionViewCollapsed()
            }
        }

        rvNoteListAdoptor.updateList(uitil.getFile(this))
        rv_notes_list?.adapter = rvNoteListAdoptor


        if (rvNoteListAdoptor.itemCount == 0){
            tvEmptyList?.visibility = View.VISIBLE
        }else if (tvEmptyList?.visibility == View.VISIBLE){
            tvEmptyList?.visibility = View.GONE
        }

        findViewById<CoordinatorLayout>(R.id.layout).clearFocus()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_items, menu)
        val searchItem : MenuItem = menu!!.findItem(R.id.btn_search)
        val searchView : SearchView? = searchItem.actionView as SearchView?
        searchView?.setOnQueryTextListener(this)
        searchView?.maxWidth = Int.MAX_VALUE
        searchView?.imeOptions = EditorInfo.IME_FLAG_NO_EXTRACT_UI

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.btn_search -> return true
        }
        return super.onOptionsItemSelected(item)
    }

}