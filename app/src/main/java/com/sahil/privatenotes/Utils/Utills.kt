package com.sahil.privatenotes.Utils

import android.content.Context
import android.widget.Toast
import com.sahil.privatenotes.R
import java.io.*

/**
 * @Author : Sahil Yadav
 * @Date : 06-04-2022
 * @Email : rao.sahab504@gmail.com
 */
class Utills {
     val TEXT_FILE_EXTTENSION : String = ".txt"



    fun getFile(context: Context): ArrayList<File> {
        val files = context.filesDir.listFiles { file, s ->
            s.lowercase().endsWith(TEXT_FILE_EXTTENSION)
        }
        return ArrayList(listOf(*files))
    }

    fun fileExists(context: Context, filename : String) : Boolean {
        val file : File =context.getFileStreamPath("$filename$TEXT_FILE_EXTTENSION")
        return file.exists()
    }
    fun writeFile(context: Context,filename: String, fileContent : String){
        try{
        val outputStreamWriter = OutputStreamWriter(context.openFileOutput("$filename$TEXT_FILE_EXTTENSION",0))
        outputStreamWriter.write(fileContent)
        outputStreamWriter.close()
        Toast.makeText(context,"fileSave",Toast.LENGTH_SHORT).show()
        }
        catch (t : Throwable){
            Toast.makeText(context,"${context.getString(R.string.exception)}$t.toString()",Toast.LENGTH_SHORT).show()
        }
    }

    fun readFile(context: Context, filename: String) : String {
        var content  = ""

        if (fileExists(context,filename)){
            try {
             //   val inputStream : InputStream = context.openFileInput("$filename$TEXT_FILE_EXTTENSION")
                val file = context.openFileInput("$filename.txt")
                val inputSR = InputStreamReader(file)
                val buffer = BufferedReader(inputSR)
                val strBuf = StringBuilder()

             //   Toast.makeText(context,buffer.readLine(),Toast.LENGTH_SHORT).show()
               // while ((buffer.readLine())!= null) {
                val str : String = buffer.readText()
                     strBuf.append(str).append("\n")
               //     Toast.makeText(context,"test"+strBuf,Toast.LENGTH_SHORT).show()
              //  }

              //  inputStream.close()
                content = strBuf.toString()
            } catch (e : Exception){
                Toast.makeText(context, context.getString(R.string.exception)+" "+e.toString(),Toast.LENGTH_SHORT).show()
            }

        }
        return content.trim()
    }

}