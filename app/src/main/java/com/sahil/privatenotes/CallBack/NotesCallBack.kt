package com.sahil.privatenotes.CallBack

import androidx.recyclerview.widget.DiffUtil
import java.io.File

/**
 * @Author : Sahil Yadav
 * @Date : 08-04-2022
 * @Email : rao.sahab504@gmail.com
 */
class NotesCallBack(oldList : MutableList<File>,newList : MutableList<File>) : DiffUtil.Callback() {
    val oldList: MutableList<File>
    val newList: MutableList<File>

    init {
        this.oldList = oldList
        this.newList = newList

    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].name.equals(newList[newItemPosition].name)
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].lastModified() < System.currentTimeMillis() - 5000
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        return super.getChangePayload(oldItemPosition, newItemPosition)
    }
}